const express=require("express")
const router=express.Router()
const createController=require("./../../controllers/creates")
const passport=require("passport")

router.get("/login",createController.loginIndex)

router.get("/register",createController.registerIndex)

router.post("/login",createController.login)

router.post("/register",createController.register)

router.get("/logout",createController.logout)

router.get("/feed",createController.feed)

module.exports=router;
