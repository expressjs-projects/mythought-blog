const passportLocal=require("passport-local-mongoose")
const mongoose=require("mongoose")
const {Schema}=mongoose;

const UserSchema=new Schema({
    name:{
        firstname:{
            type:String,
            required:true
        },
        lastname:{
            type:String,
            required:true
        }
    },
    username:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    date:{
        type:Date,
        default:Date.now
    }
})

UserSchema.plugin(passportLocal,{
    usernameField:"email"
})

module.exports=mongoose.model("User",UserSchema)