const { name } = require("ejs");
const User=require("../model/User") 
const passport=require("passport")
module.exports={
    loginIndex:(req,res)=>{
            res.render("login")
    },
    registerIndex:(req,res)=>{
        // render register page
        // if(passport.authorize()){
            // res.redirect("feed")
        // }else{
            res.render("register")
        // }
    },
    register:(req,res,next)=>{
        let {firstname,lastname,email,password,username}=req.body;
        let newUser=new User({
            name:{
                firstname:firstname,
                lastname:lastname
            },
            email:email,
            username:username,
            password:password
        })
        User.register(newUser,password,(err,user)=>{
            if(user){
                req.flash("success",`user ${email} successfully registered`)
                console.log(`user ${email} successfully registered`)
                // res.redirect('/feed')
                console.log(user)
                next()
            }else{
                req.flash("error",`user not successfully registered, ${err.message}`)
                console.log(err.message)
                // res.redirect('/login')
                next()
            }
        })
    },
    login:passport.authenticate("local",{
        failureRedirect:'/login',
        failureFlash:"Error loggin in",
        failureMessage:"Error loggin in",
        successRedirect:"/feed",
        successMessage:"successfully logged in",
        successFlash:"successfully logged in"
    }),
    feed:(req,res)=>{
        // if(passport.authorize()){
            res.render("feed")
        // }else{
            // res.redirect("/login")
        // }
        
    },
    logout:(req,res)=>{
        req.logout()
        req.flash("success","successfully logged out")
        res.redirect("/login")

    }
}