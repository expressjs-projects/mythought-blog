/* 
Import required modules here
*/
const express=require("express")
const app=express()
const passport=require("passport")
const expressSession=require("express-session")
const cookieParser=require("cookie-parser")
const connectFlash=require("connect-flash")

// Mongoosedb
const mongooseConfig=require("./config/mongoose.config")
const secret=require("./config/keys").secret
// Middlewares
// Set ejs engine
app.set("view engine","ejs")

app.use(express.urlencoded({extended:false}))
app.use(express.json())

app.use(cookieParser(secret))
app.use(expressSession({
    secret:secret,
    cookie:{
        maxAge:4000000
    },
    resave:false,
    saveUninitialized:false
}))

// Passport config
app.use(passport.initialize())
app.use(passport.session())

const User=require("./model/User")
passport.use(User.createStrategy())
passport.serializeUser(User.serializeUser())
passport.deserializeUser(User.deserializeUser())



app.use(connectFlash())

app.use((req,res,next)=>{
    
    res.locals.flashMessages=req.flash()
    next()
})
const routes=require("./routes/reg/creates")
app.use(routes)

const PORT=process.env.PORT|| 5000
app.listen(PORT,()=>{
    console.log(`application runnning on 3000`)
})