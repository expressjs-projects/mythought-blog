const mongoose=require("mongoose")
const db=require("./keys").MongoURI

mongoose.connect(db,{useNewUrlParser:true}).then(()=>{
    console.log(`Database successfully connected to mongo Atlas`)
}).catch(err=>{
    console.log(err)
})